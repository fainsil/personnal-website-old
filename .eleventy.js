const pluginNavigation = require("@11ty/eleventy-navigation");
const markdownItAttrs = require("markdown-it-attrs");
const autoprefixer = require("autoprefixer");
const markdownIt = require("markdown-it");
const htmlmin = require("html-minifier");
const purgecss = require("purgecss");
const postcss = require("postcss");
const yaml = require("js-yaml");
const path = require("path");
const sass = require("sass");
const fs = require("fs");
const markdownLib = markdownIt({
  html: true,
  breaks: true,
  linkify: true,
}).use(markdownItAttrs);

const pattern = new RegExp("(<style>)(.*)?(</style>)", "gm");

module.exports = function (eleventyConfig) {
  eleventyConfig.addDataExtension("yml", (contents) => yaml.load(contents));
  eleventyConfig.addGlobalData("username", "Laureηt");
  eleventyConfig.addWatchTarget("./src/scss/");
  eleventyConfig.setLibrary("md", markdownLib);
  eleventyConfig.addPlugin(pluginNavigation);
  eleventyConfig.addPassthroughCopy({
    "assets/": "./",
  });

  eleventyConfig.on("eleventy.before", async () => {
    // iterate over scss files
    let files = fs.readdirSync("./src/scss/");
    files.forEach((file) => {
      // Compile SASS
      const result = sass.compile("./src/scss/".concat(file), {
        style: "compressed",
        sourceMap: false,
      });
      console.log(`[SCSS] ${file} compiled`);

      // Optimize and write file with PostCSS
      postcss([autoprefixer])
        .process(result.css.toString())
        .then((result) => {
          fs.writeFile(
            "./src/_includes/css/".concat(path.parse(file).name, ".css"),
            result.css,
            (err) => {
              if (err) throw err;
              console.log(`[POSTCSS] ${file} optimized`);
            }
          );
        });
    });
  });

  eleventyConfig.addTransform("htmlmin", function (content, outputPath) {
    if (
      process.env.NODE_ENV === "production" &&
      outputPath &&
      outputPath.endsWith(".html")
    ) {
      const minified = htmlmin.minify(content, {
        collapseWhitespace: true,
        useShortDoctype: true,
        removeComments: true,
        minifyCSS: true,
        minifyJS: true,
      });
      return minified;
    } else {
      return content;
    }
  });

  eleventyConfig.addTransform(
    "purgestyle",
    async function (content, outputPath) {
      if (
        process.env.NODE_ENV === "production" &&
        outputPath &&
        outputPath.endsWith(".html")
      ) {
        const [{ css: result }] = await new purgecss.PurgeCSS().purge({
          content: [
            { raw: content.replace(pattern, "$1$3"), extension: "html" },
          ],
          css: [{ raw: pattern.exec(content)[2] }],
        });
        console.log(`[PURGECSS] ${outputPath} purged`);
        return content.replace(pattern, `<style>${result}</style>`);
      }
      return content;
    }
  );

  eleventyConfig.addShortcode("year", () => {
    const year = new Date().getFullYear();
    return year.toString();
  });

  eleventyConfig.addFilter("renderMarkdown", function (rawString) {
    return markdownLib.renderInline(rawString);
  });

  return {
    markdownTemplateEngine: "njk",
    passthroughFileCopy: true,
    dir: {
      includes: "_includes",
      output: "_site",
      data: "_data",
      input: "src",
    },
  };
};
