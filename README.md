# Laurent Fainsin's Personnal website

## Built with

- [EditorConfig](https://editorconfig.org/)
- [VS Code](https://code.visualstudio.com/)
- [Nunjuck](https://mozilla.github.io/nunjucks/)
- [11ty.js](https://www.11ty.dev/)
- [Yarn](https://yarnpkg.com/)
- [Sass](https://sass-lang.com/)

## Getting started

### Prerequisites

[Yarn](https://yarnpkg.com/) should manage every dependencies for you. \
It is recommended to use [VS Code](https://code.visualstudio.com/) with these extensions :

- [Nunjucks Template Formatter](https://marketplace.visualstudio.com/items?itemName=okitavera.vscode-nunjucks-formatter)
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [HTML CSS Support](https://marketplace.visualstudio.com/items?itemName=ecmel.vscode-html-css)
- [Nunjucks](https://marketplace.visualstudio.com/items?itemName=ronnidc.nunjucks)
- [Beautify](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify)

### Installation

Clone the repository :

```bash
git clone git@git.inpt.fr:fainsil/personnal-website.git
```

Install the dependencies :

```bash
cd personnal-website
yarn install
```

### Usage

To run locally the website on your machine press F5 in vscode or use :

```bash
yarn run start
```

To build the website for deployment :

```bash
yarn run build
```

## Contributing

This repository is under the [Contributing Covenant](https://www.contributor-covenant.org/) code of conduct.
See [`CONTRIBUTING.md`](https://git.inpt.fr/fainsil/personnal-website/-/blob/master/CONTRIBUTING.md) for more information.\
Please use [conventional commits](https://www.conventionalcommits.org/).

## License

Distributed under the [MIT](https://choosealicense.com/licenses/mit/) license.
See [`LICENSE`](https://git.inpt.fr/fainsil/personnal-website/-/blob/master/LICENSE) for more information.

## Contact

Laurent Fainsin \<[laurentfainsin@protonmail.com](mailto:laurentfainsin@protonmail.com)\>
