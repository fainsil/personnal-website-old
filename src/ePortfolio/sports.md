---
layout: eportfolio.njk
eleventyNavigation:
  key: sport
  title: Hobbies
  order: 4
---

# Sports

I try to experience as much of them as I can when I have the opportunity, though for the moment I've mainly did Climbing and Archery. I had to stop doing sports during my Prépa because of the lack of time, but now that this is over I will do my best to improve my shape.

# Hobbies

Obviously I love Computer Science since I decided to go in an engineering school specialized in it. Despite this I also like to fiddle around with electronics and to fool around on the internet. However I reassure you, I also like calm activities like taking care of my beloved aquarium and plants.
