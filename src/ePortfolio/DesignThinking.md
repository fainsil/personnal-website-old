---
layout: eportfolio.njk
eleventyNavigation:
  key: dt
  title: DT
  order: 7
---

Here is a short video I had to make to present my future plans, by applying the design thinking methodology :

<div style="position:relative;padding-bottom:56.25%;margin-bottom:2rem;">
  <video src="{{ '/content/2021-03-14-dt.mp4' | url }}" style="width:100%;height:100%;position:absolute;left:0px;top:0px;" frameborder="0" width="100%" height="100%" controls></video>
</div>
