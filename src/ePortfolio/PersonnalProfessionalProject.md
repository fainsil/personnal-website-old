---
layout: eportfolio.njk
eleventyNavigation:
  key: pp
  title: PPP
  order: 8
---

## Slides

<div style="position:relative;padding-bottom:56.25%;margin-bottom:2rem;">
  <iframe src="{{ '/content/PPP/index.html' | url }}" style="width:100%;height:100%;position:absolute;left:0px;top:0px;" frameborder="0" width="100%" height="100%"></iframe>
</div>

## First year Internship report

Here is the report I wrote after I did my first year internship.

<div style="position:relative;padding-bottom:141%;">
  <object data="{{ '/content/PPP/1A_report.pdf' | url }}" style="width:100%;height:100%;position:absolute;left:0px;top:0px;" width="100%" height="100%" type="application/pdf">
    <a href="{{ '/content/PPP/1A_report.pdf' | url }}">Click here to access the pdf version.</a>
  </object>
</div>
