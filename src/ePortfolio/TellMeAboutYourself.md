---
layout: eportfolio.njk
eleventyNavigation:
  key: tmay
  title: TMAY
  order: 5
---

Here are small videos I had to make to present myself.

## English Version

<div style="position:relative;padding-bottom:56.25%;margin-bottom:2rem;">
  <video src="{{ '/content/2020-11-06-v1.mp4' | url }}" style="width:100%;height:100%;position:absolute;left:0px;top:0px;" frameborder="0" width="100%" height="100%" controls></video>
</div>

## Japanese Version

<div style="position:relative;padding-bottom:56.25%;margin-bottom:2rem;">
  <video src="{{ '/content/2020-12-19-v1.mp4' | url }}" style="width:100%;height:100%;position:absolute;left:0px;top:0px;" frameborder="0" width="100%" height="100%" controls></video>
</div>
