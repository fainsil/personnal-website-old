---
layout: eportfolio.njk
eleventyNavigation:
  key: ci
  title: International
  order: 10
---

Actuellement en troisième année à l'ENSSEIHT, je suis inscrit dans la filière Image et Multimédia et j'ai également un double diplôme de master en recherche PSMSC. En dehors de mes études à l'ENSSEIHT, j'ai passé deux mois aux États-Unis en séjournant chez une famille américaine et j'ai effectué quelques voyages de courte durée en Suisse, en Espagne et en Allemagne. Cependant, je n'ai pas encore effectué 16 semaines de mobilité académique.

En ce qui concerne l'échange académique en Suisse, je devais initialement partir à l'EPFL lors du semestre S9, mais malheureusement l'accord entre les deux établissements a pris fin et je n'ai pas pu y aller. Après discussion avec mes professeurs pour savoir si je pouvais quand même postuler, nous avons constaté que la plupart des cours qui m'intéressaient chez eux étaient en fait similaires à ceux que j'avais déjà suivis au S8 à l'ENSSEIHT ou ceux que je suis actuellement en train de suivre au S9 à l'ENSSEIHT.

En ce moment, je suis activement à la recherche d'un stage PFE orienté vers la recherche à l'étranger. Je suis en contact avec de nombreuses entreprises, mais je n'ai pas encore trouvé de proposition concrète. Cependant, j'ai déjà reçu une offre de Safran pour travailler dans leur département de recherche et développement à Paris. Le stage se déroulerait en anglais, à la fois à l'oral et à l'écrit, et il est possible que je puisse travailler à distance dans un pays étranger.

Lors de mon stage de deuxième année à l'IRIT, j'ai eu la chance de lire de nombreux articles scientifiques en anglais et j'ai co-écrit un article en anglais qui est en cours de publication. De plus, en tant que membre de net7, l'association informatique de l'ENSSEIHT, j'ai travaillé avec des technologies en anglais et j'ai collaboré à des projets open source sur GitHub en ligne avec d'autres contributeurs, tous en anglais bien sûr.
