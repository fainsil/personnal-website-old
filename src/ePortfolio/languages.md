---
layout: eportfolio.njk
eleventyNavigation:
  key: lang
  title: Languages
  order: 3
---

# Professional english

I have always been in the top of my English classes. I currently have not passed any official certifications (like the TOEIC or the TOEFL), but this will change soon. I've spent countless hours on the internet, I've thus aggregated a wide variety of vocabulary and expressions that can prove themselves useful in quite a number of situations. This doesn't necessarily means that I'm comfortable with the idea of speaking English in public (since my tongue still struggles against some weird phonetics), but I've had my share of talks (from various presentations, mainly from high school) so I know how to lead a meeting or a debate.

# Spanish

I studied Spanish during middle school and high school. It's been quite some time since I last opened a Spanish textbook so I must be a bit rusty, but give me a couple of days and I should be back on my feet. Right now I can at least understand a basic conversation and engage in it.

# Japanese

I started studying Japanese recently at my engineering school. I chose Japanese language since it is vastly different from the Latin based ones, as is its culture, from which I can most likely learn a thing or two. My goal is to get a certification and to study abroad in Japan during the international exchange during my second year (M1).

Here is a presentation I did during my classes:

<div style="position:relative;padding-bottom:56.25%;margin-bottom:2rem;">
  <iframe src="{{ '/content/jap/index.html' | url }}" style="width:100%;height:100%;position:absolute;left:0px;top:0px;" frameborder="0" width="100%" height="100%"></iframe>
</div>

# French

Well I'm native, so I'm fluent in baguette and croissant.

# International mobility

I have been to the United States several times over periods of one month. Once during Summer 2015 in New York and another time during Summer 2017 in Los Angeles. I went overseas with a travel agency that offered students the opportunity to improve their English. We were divided into groups according to our level, I was always in the "advanced" group so I mostly did these trips to discover America from my very own eyes. Indeed, half of the time was dedicated to studying English and the other for visiting the city and the various activities that it offered. The key feature about theses trips was that we were placed in Americans families, thus I learned a lot about the American culture and I improved a lot my speaking.
