---
layout: eportfolio.njk
eleventyNavigation:
  key: ce
  title: CE
  order: 6
---

This is the civic engagement that I am currently pursuing during my studies at ENSSEIHT.

## Flyer

<div style="position:relative;padding-bottom:141%;">
  <object data="{{ '/content/CE.pdf' | url }}" style="width:100%;height:100%;position:absolute;left:0px;top:0px;" width="100%" height="100%" type="application/pdf">
    <a href="{{ '/content/CE.pdf' | url }}">Click here to access the pdf version.</a>
  </object>
</div>
