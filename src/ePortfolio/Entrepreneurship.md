---
layout: eportfolio.njk
eleventyNavigation:
  key: index
  title: ENTR
  order: 9
---

# Entrepreneurship

Since the 4th of May 2021 and for one year, I hold the position of Treasurer at net7 and President at INP-net.
My duties as Treasurer of net7 and President of INP-net will end soon.

## What is net7/INP-net ?

For more than twenty years, net7 has been led by keen and energetic students to serve the needs of the campus. It started in 1997, when a few students had the idea to create a network between the engineering schools of Toulouse, what’s known as Toulouse INP. Thus INP-net was born. Two years later, in 1999, the Windows, macOS and Linux clubs of ENSEEIHT merged together to give net7, the ENSEEIHT branch of INP-net. Since then, the two clubs are closely related, the members of one are also members of the other, and the entire infrastructure is managed from net7's premises at ENSEEIHT. Since 2016, net7 is officially declared as an “Association loi 1901”, the French equivalent of nonprofit organizations.

## What I've done at net7

I currently spend all of my Thursday afternoons at the club improving or maintaining the services we offer. I also often spend my breaks between classes in the club room. Here is a list of what I've acomplished at the association :

- Financial handover
- Improvement of the ecobox service on the portail
- Wiki migration (used by all technical clubs)
- Update of the minecraft server, regularly used by a dozen people
- Creation of a collaborative GBA emulator for preshows
- Moderation of mailing lists
- Managing the club's spending on new equipment
- Reorganization of our archives
- Creation of a hypervisor cluster with proxmox
- Migration of some of our services into virtual machines, or into the kubernetes cluster
- Replacement of our file service for students, ftp -> nextcloud
- Organization and purchase of new hardware for our brand new server room
- Organization of a git training for a few dozen students
- Organization of a git training for a few dozen students
- Did a root training for future club members
- Filled various tickets and communicated with the STI for the maintenance of our club room
- Participated to the Toulouse Hacking Convention for 3 days
- Currently laying the foundations of our new webapp with the help of previous members
- Help a couple of students with computer problems
- Organized the General Assembly of net7
- Wrote the minutes of the General Assembly of net7
- Planning a ton of work for this summer to further improve our services

## Conclusion

My time at net7 was very instructive for me, I greatly improved my technical abilities, but also my soft skills through the crucial communication with other clubs.
