---
layout: eportfolio.njk
eleventyNavigation:
  key: index
  title: Homepage
  order: 1
---

# About me

Hi, my name's Laurent Fainsin, and I'm currently an M1 engineering student at [ENSEEIHT](https://www.enseeiht.fr/) in France. I study [Computer Science](https://www.enseeiht.fr/fr/formation/formation-ingenieur/departement-sn/programme-sn.html) and this is my personnal ePortfolio.

# Socials & Contacts

You can email me at :

- [laurentfainsin@<WBR>protonmail.com](mailto:laurentfainsin@protonmail.com)
- [laurent.fainsin@<WBR>etu.inp-n7.fr](mailto:laurent.fainsin@etu.inp-n7.fr)

You can reach me via :

- [Matrix.to](https://matrix.to/#/@fainsil:inpt.fr)
- [LinkedIn](https://www.linkedin.com/in/laurent-fainsin/)
