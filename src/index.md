---
layout: layout.njk
---

{% block head %}

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css" integrity="sha256-gsmEoJAws/Kd3CjuOQzLie5Q3yshhvmo7YNtBG7aaEY=" crossorigin="anonymous">

{% endblock head %}

# whoami {data-chaffle="en"}

Hi, my name's Laurent Fainsin, and I'm currently an M1 [engineering](https://www.francecompetences.fr/recherche/rncp/35713/) student at [ENSEEIHT](https://www.enseeiht.fr/) in France. I study [Computer Science](https://www.enseeiht.fr/fr/formation/formation-ingenieur/departement-sn/programme-sn.html) and this is my personnal website to showcase my work. Here is my [resume]({{ '/resume/en' | url }}) if you are professionally interested.

---

# ls projects {data-chaffle="en"}

{% for key, project in resumes.en.personnal_projects.list %}

  <section>
    <h3>{{ key }}</h3>
    {{- project.description | renderMarkdown | safe -}}
    {%- if project.pdf or project.repo -%}
    <div class="project-links">
      {%- if project.pdf -%}
        <a class="fa fa-file-pdf-o" href="{{ 'project.pdf' | url }}"></a>
      {%- endif -%}
      {%- if project.repo -%}
        <a class="fa fa-git" href="{{ project.repo }}"></a>
      {%- endif -%}
    </div>
    {%- endif -%}
  </section>

{% endfor %}

---

# cat contacts {data-chaffle="en"}

You can email me at :

- [laurentfainsin@<WBR>protonmail.com](mailto:laurentfainsin@protonmail.com) ([PGP]({{ '/public.pgp' | url }}))
- [laurent.fainsin@<WBR>etu.inp-n7.fr](mailto:laurent.fainsin@etu.inp-n7.fr)

You can reach me via :

- [Matrix.to](https://matrix.to/#/@fainsil:inpt.fr)
- [LinkedIn](https://www.linkedin.com/in/laurent-fainsin/)
